const express = require('express');
const app = express();

app.use(express.json())

const subscribers = require('./subscribers.json')
const courses = require('../courses/courses.json')

app.listen(80, ()=> {
    console.log('Hello Express!');
})

// get all the subscribers
app.get('/subscribers' ,(req, res) => {
    res.status(200).json(subscribers)
})


// get subscriber that match the id picked 
app.get('/subscribers/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const subscriber = subscribers.find(subscriber => subscriber.id === id)
    res.status(200).json(subscriber)
})


// post a subscriber
app.post('/subscribers',(req, res)=>{
    subscribers.push(req.body)
    res.status(200).json(subscribers)
})


// Update a subscriber with  new informtions 
app.put('/subscribers/:id', (req, res)=>{
    const id = parseInt(req.params.id)
    let subscriber = subscribers.find(subscriber => subscriber.id === id)
    subscriber.subscriber = req.body.subscriber,
    subscriber.price = req.body.price,
    res.status(200).json(subscriber)
})


// Delete a subscriber 
app.delete('/subscribers/:id', (req, res) => {
    const id = parseInt(req.params.id)
    let subscriber = subscribers.find(subscriber => subscriber.id === id)
    subscribers.splice(subscribers.indexOf(subscriber), 1)
    res.status(200).json(subscribers)
})


//Show subscribers that belongs to a course
app.get('/subscribers-course/:id',(req, res)=> {
    const id = parseInt(req.params.id)
    const coursesSubscribers = subscribers.filter(subscriber => subscriber.idCourse === id)
    res.status(200).json(coursesSubscribers)
})

//Show Course that has the following subscriber
app.get('/course-subscriber/:id',(req, res)=> {
    const id = parseInt(req.params.id)
    const subscriber = subscribers.find(subscriber => subscriber.id === id)
    const course = courses.find(course => course.id === subscriber.idCourse)
    res.status(200).json(course)
})


//Show subscriber infos 
app.get('/subscriber/:name',(req, res)=> {
    const name = req.params.name
    const subscriber = subscribers.find(subscriber => subscriber.nom === name)
    res.status(200).json(subscriber)
})