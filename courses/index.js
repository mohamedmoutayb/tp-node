const express = require('express');
const app = express();

app.use(express.json())

const courses = require('./courses.json')

app.listen(82, ()=> {
    console.log('Hello Express!');
})

// get all the courses
app.get('/courses' ,(req, res) => {
    res.status(200).json(courses)
})


// get course that match the id picked 
app.get('/courses/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const course = courses.find(course => course.id === id)
    res.status(200).json(course)
})


// post a course
app.post('/courses',(req, res)=>{
    courses.push(req.body)
    res.status(200).json(courses)
})


// Update a course with  new informtions 
app.put('/courses/:id', (req, res)=>{
    const id = parseInt(req.params.id)
    let course = courses.find(course => course.id === id)
    course.course = req.body.course,
    course.price = req.body.price,
    res.status(200).json(course)
})


// Delete a course 
app.delete('/courses/:id', (req, res) => {
    const id = parseInt(req.params.id)
    let course = courses.find(course => course.id === id)
    courses.splice(courses.indexOf(course), 1)
    res.status(200).json(courses)
})